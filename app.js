const btnSubmit = document.querySelector('button[type="submit"]');
const addInput = document.querySelector('#task_name');
const todoList = document.querySelector('.todo-list');
const doneList = document.querySelector('.done-list');

btnSubmit.addEventListener('click', (e)=> {
   e.preventDefault();
   let newLi = document.createElement('li');
   newLi.classList.add('flow-text');
   newLi.innerHTML = `<span>${addInput.value}</span>`;

   let deleteBtn = document.createElement('button');
   deleteBtn.textContent = 'Delete task';
   deleteBtn.classList = 'btn red darken-4';
   deleteBtn.addEventListener('click', deleteTask);

   let editBtn = document.createElement('button');
   editBtn.textContent = 'Edit task';
   editBtn.classList = 'btn lime';
   editBtn.addEventListener('click', editTask);

   let saveBtn = document.createElement('button');
   saveBtn.textContent = 'Save task';
   saveBtn.classList = 'btn green';
   saveBtn.style.display = 'none';
   saveBtn.addEventListener('click', saveTask);

   newLi.insertAdjacentHTML('beforeend',
       `<label><input type="checkbox"><span></span></label>`);
   newLi.appendChild(deleteBtn);
   newLi.appendChild(editBtn);
   newLi.appendChild(saveBtn);
   todoList.appendChild(newLi);

   let checkbox = newLi.querySelector('input');
   checkbox.addEventListener('click', changeStatus)
   addInput.value = '';
});

function editTask(e) {
   const ourLi = e.target.parentNode;
   const editBtn = ourLi.querySelector('.lime');
   editBtn.style.display = 'none';
   const saveBtn = ourLi.querySelector('.green');
   saveBtn.style.display = 'inline-block';

   const text = ourLi.querySelector('span');
   text.outerHTML = `<input type="text" value=${text.textContent} />`
}

function saveTask(e) {
   const ourLi = e.target.parentNode;
   const editBtn = ourLi.querySelector('.lime');
   editBtn.style.display = 'inline-block';
   const saveBtn = ourLi.querySelector('.green');
   saveBtn.style.display = 'none';

   const input = ourLi.querySelector('input[type="text"]');
   input.outerHTML = `<span>${input.value}
</span>`
}

function deleteTask(e) {
   let checkbox = e.target.parentNode.querySelector('input');
   checkbox.checked
       ? doneList.removeChild(e.target.parentNode)
       : todoList.removeChild(e.target.parentNode)
}
function changeStatus(e) {
   if(e.target.checked) {
      todoList.removeChild(e.target.parentNode.parentNode)
      doneList.appendChild(e.target.parentNode.parentNode)
   } else {
      doneList.removeChild(e.target.parentNode.parentNode)
      todoList.appendChild(e.target.parentNode.parentNode)
   }
}

